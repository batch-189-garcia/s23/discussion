console.log("hello-s23");

/*

OBJECTS
	an object is a data type that is used to represnt a real world object. it is also a collection of related data and/or functionalities.


Creating objects using object literal
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB

		}

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log('Result from creating objects');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function 

/*
	Creates a reusable function to create a several objects that have the same data structure. this is useful for creating multiple instances/copies of an object.

	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA,
			this.keyB = valueB
		}
	
*/

function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using object constructors");
console.log(laptop);


let myLaptop = new Laptop("macbook Air", [2020, 2021]);
console.log(myLaptop);

let oldLaptop = Laptop('Portal', 2020);
console.log("Result from creating w/out the new keyword");
console.log(oldLaptop);

// 2 ways of Creating empty objects
let computer = {}
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

// Accesing Object Property

// using the dot notation - preferred
console.log("Result from dot notation " + myLaptop.name);

// using the square bracket notation - could be confuse as array
console.log("Result from square bracket notation: " + myLaptop["name"]);

// Accessing array objects
let array = [laptop, myLaptop]

// square bracket notation
console.log(array[0]["name"])

// Dot notation
console.log(array[0].name);

// Initializing / Adding / Deleting / Reassigning Object Properties
let car = {}
console.log(car)

car.name = "Honda Civic";
console.log("result from adding property using dot notation")
console.log(car);

car["manufacture date"] = 2019
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("result from deleting object properties: ");
console.log(car);

// Reassigningobject properties
car.name = "Tesla";
console.log("Result from reassigning property: ")
console.log(car);

// Object Methods
/*
	A method is a function which is a property of an object. They are also function and one of the key differences they have is that the methods are functions related to a specific object.

*/

let person = {
	name: "John",
	talk: function () {
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person);
console.log("Result from object methods: ");
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk();

let friend = {
	firstName: "Nehemiah",
	lastName: "Ellorico",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["nejellorico@mail.com", "nej123@mail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}

}

friend.introduce();

// Real World Application Objects
/*
	Scenario:
		1. We would like to create a game that would have several pokemon interact with each other.
		2. Every pokemon would have the same set of stats, properties and functions

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");

	},
	faint: function() {
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);

// Creating an object constructor
function Pokemon (name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){


		console.log(this.name + " tackled " + target.name);

		// Added  below
		console.log("target.health before tackle is " + target.health);
		console.log("this.attack power is " + this.attack);
		
		// Added below line
		target.health = target.health - this.attack; 
		
		console.log(target.name + "'s health is now reduced to " + (target.health));

		// Added  below
		console.log("target.health is " + target.health);
		
		if (target.health <= 5) {target.faint()}


	}, 
	
	this.faint = function() {
		console.log(this.name + " fainted. ")
	}


}


let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon ("Squirtle", 8);

console.log(pikachu);
console.log(squirtle);

console.log("tackle1");

pikachu.tackle(squirtle);
console.log(squirtle);

console.log("tackle2");
pikachu.tackle(squirtle);
console.log(squirtle);

// squirtle.faint();


/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function


*/